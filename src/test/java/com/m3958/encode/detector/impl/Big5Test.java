package com.m3958.encode.detector.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import org.junit.Test;

import com.m3958.encode.detector.BaseTe;
import com.m3958.encode.detector.DetectResult;
import com.m3958.encode.detector.exception.FileOpenException;
import com.m3958.encode.detector.util.ByteUtil;

public class Big5Test extends BaseTe {

    /*
     * 保留（用作造字区） 8140H－A0FEH
     * 标点符号、希腊字母及特殊符号 A140H－A3BFH
     * 保留（未开放用于造字区） A3C0H－A3FEH
     * 常用汉字（先按笔划，再按部首排序） A440H－C67EH
     * 保留（用作造字区） C6A1H－C8FEH
     * 非常用汉字（先按笔划，再按部首排序） C940H－F9D5H
     * 保留（用作造字区） F9D6H－FEFEH
     */
    @Test
    public void t() throws CharacterCodingException {
        printme(0x8140 + ":" + 0xA0FE);
        printme((short) 0x8140 + ":" + (short) 0xA0FE);
        printme(0xA140 + ":" + 0xA3BF);
        printme((short) 0xA140 + ":" + (short) 0xA3BF);
        printme(0xA3C0 + ":" + 0xA3FE);
        printme((short) 0xA3C0 + ":" + (short) 0xA3FE);

        printme(0xA440 + ":" + 0xC67E);
        printme((short) 0xA440 + ":" + 0xC67E);

        printme(0xC6A1 + ":" + 0xC8FE);
        printme((short) 0xC6A1 + ":" + (short) 0xC8FE);

        printme(0xC940 + ":" + (short) 0xF9D5);
        printme((short) 0xC940 + ":" + (short) 0xF9D5);

        printme(0xF9D6 + ":" + (short) 0xFEFE);
        printme((short) 0xF9D6 + ":" + (short) 0xFEFE);

        byte b1 = (byte) 0x81;
        byte b2 = (byte) 0x40;

        byte[] bytes = { b1, b2 };

        ByteBuffer bb = ByteBuffer.wrap(bytes);
        short s = bb.asShortBuffer().get();
        assertThat(s, is((short) 0x8140));
    }

    @Test
    public void legal() throws UnsupportedEncodingException, FileOpenException {
        byte[] bytes = "本網站通過A+優先等級無障礙網頁檢測".getBytes("BIG5");
        
        printme(ByteUtil.bytesToHex(bytes));

        Big5 detector = new Big5(bytes);
        DetectResult dr = detector.detect().result();
        
        assertThat("failed should be zero", dr.getFailedNumber(), is(0));
        assertThat("percent should be 100", dr.successPercent(), is(100));
        assertThat("should detect 2 asciis", dr.getAsciiNumber(), is(2));
        assertThat("should detect 2 chinese", dr.getCharNumber(), is(16));
    }

}
