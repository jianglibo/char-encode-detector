package com.m3958.encode.detector;

public class DetectResult {
    
    private int asciiNumber;
    
    private int charNumber;
    
    private int successedNumber;
    
    private int failedNumber;
    
    private String charsetName;

    public DetectResult(String charsetName, int asciiNumber, int charNumber, int successedNumber, int failedNumber) {
        super();
        this.charsetName = charsetName;
        this.asciiNumber = asciiNumber;
        this.charNumber = charNumber;
        this.successedNumber = successedNumber;
        this.failedNumber = failedNumber;
    }
    
    public int asciiPercent() {
        if ((asciiNumber + charNumber) == 0) {
            return 0;
        }
        return Math.round(((float)asciiNumber / (float)(charNumber + asciiNumber)) * 100);
    }
    
    
    public int successPercent() {
        if ((successedNumber + failedNumber) == 0) {
            return 0;
        }
        return Math.round(((float)successedNumber / (float)(successedNumber + failedNumber)) * 100);
    }

    public int getAsciiNumber() {
        return asciiNumber;
    }

    public void setAsciiNumber(int asciiNumber) {
        this.asciiNumber = asciiNumber;
    }

    public int getCharNumber() {
        return charNumber;
    }

    public void setCharNumber(int charNumber) {
        this.charNumber = charNumber;
    }

    public int getSuccessedNumber() {
        return successedNumber;
    }

    public void setSuccessedNumber(int successedNumber) {
        this.successedNumber = successedNumber;
    }

    public int getFailedNumber() {
        return failedNumber;
    }

    public void setFailedNumber(int failedNumber) {
        this.failedNumber = failedNumber;
    }

    public String getCharsetName() {
        return charsetName;
    }

    public void setCharsetName(String charsetName) {
        this.charsetName = charsetName;
    }
}
