package com.m3958.encode.detector;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author jianglibo@gmail.com
 *         2015年12月1日
 *
 */
public abstract class BaseTe {
    
    protected Path fixtureOut = Paths.get("fixtures", "outputs");

    protected void printme(Object o) {
        System.out.println(o);
    }
    
    protected void printme(String prefix, Object o) {
        System.out.print(prefix + ":");
        System.out.println(o);
    }
}
