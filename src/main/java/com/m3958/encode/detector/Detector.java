package com.m3958.encode.detector;

import com.m3958.encode.detector.exception.FileOpenException;

public interface Detector {
    
    DetectResult result();
    
    Detector detect() throws FileOpenException;

}
