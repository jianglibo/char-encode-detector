package com.m3958.encode.detector.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import org.junit.Test;

import com.m3958.encode.detector.BaseTe;
import com.m3958.encode.detector.DetectResult;
import com.m3958.encode.detector.exception.FileOpenException;

public class Utf8Test extends BaseTe {

    @Test
    public void legal() throws UnsupportedEncodingException, FileOpenException {
        byte[] bytes = "a号bc有".getBytes(StandardCharsets.UTF_8);

        UTF8 detector = new UTF8(bytes);
        DetectResult dr = detector.detect().result();
        assertThat("failed should be zero", dr.getFailedNumber(), is(0));
        assertThat("percent should be 100", dr.successPercent(), is(100));
        assertThat("should detect 3 asciis", dr.getAsciiNumber(), is(3));
        assertThat("should detect 2 chinese", dr.getCharNumber(), is(2));
    }

    @Test
    public void detectFileLarge() throws IOException, FileOpenException {
        if (!Files.exists(fixtureOut)) {
            Files.createDirectories(fixtureOut);
        }
        Path path = fixtureOut.resolve("gb2312.txt");
        Writer wt = Files.newBufferedWriter(path, StandardCharsets.UTF_8, StandardOpenOption.WRITE, StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
        int writed = 0;

        while (writed < 10000) {
            wt.write("a号bc有");
            writed++;
        }
        wt.flush();
        wt.close();

        UTF8 detector = new UTF8(path);
        DetectResult dr = detector.scanWholeFile().detect().result();
        assertThat("success percent should be 100%", dr.successPercent(), is(100));
    }

}
