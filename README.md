#char-encode-detector

a utility to detect char encode.

## 开始
```
git clone https://git.oschina.net/jianglibo/char-encode-detector.git
cd char-encode-detector
复制gradle.properties.template成gradle.properties
gradle.bat eclipse
```

## implemented detectors （目前实现的编码）
* GB2312
* GBK
* UTF-8

## all object are not thread safe. (所有对象是线程不安全，new后使用后丢弃。)

## usage

```
        byte[] bytes = "a号bc有".getBytes("gb2312");
        DetectResult dr = Detectors.create().detect(bytes).result();
        assertThat("success percent should be 100%", dr.successPercent(), is(100));
        assertThat("should detect 3 ascii chars", dr.getAsciiNumber(), is(3));
        assertThat("charset name should be gb2312" ", dr.getCharsetName().toLowerCase(), is(true));
```

所有的配置在detect之前设置：

```
	Detectors.create().scanWholeFile().prefer(LanguageName.CHINESE).detect(somepath);
```

完成的非常有限，欢迎fork和pullrequest。