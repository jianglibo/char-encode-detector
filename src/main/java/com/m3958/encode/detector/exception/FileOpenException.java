package com.m3958.encode.detector.exception;

public class FileOpenException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -3369017151096328679L;
    
    public FileOpenException(String msg) {
        super(msg);
    }

}
