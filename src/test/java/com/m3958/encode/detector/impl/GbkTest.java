package com.m3958.encode.detector.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import org.junit.Test;

import com.m3958.encode.detector.BaseTe;
import com.m3958.encode.detector.DetectResult;
import com.m3958.encode.detector.exception.FileOpenException;
import com.m3958.encode.detector.util.ByteUtil;

public class GbkTest extends BaseTe{

    @Test
    public void t() throws CharacterCodingException {
        
        char h = '\u6C49'; // 汉

        CharBuffer cb = CharBuffer.allocate(1);
        cb.put(h);
        cb.flip();
        
        Charset gbk = Charset.forName("gbk");

        CharsetEncoder encoder = gbk.newEncoder();
        ByteBuffer bb = encoder.encode(cb);
        
        assertThat("gbk is two byte.", bb.limit(), is(2));
        
        byte[] ba = new byte[bb.limit()];
        bb.get(ba);
        assertThat(ByteUtil.bytesToHex(ba), is("BABA"));
        
        Gb2312 detector = new Gb2312(ba);
        
        assertThat("byte value should in gb2312 range.", detector.isGb2312FirstByte(ba[0]), is(true));
        assertThat("byte value should in gb2312 range.", detector.isGb2312SecondByte(ba[1]), is(true));
    }
    
    @Test
    public void legal() throws UnsupportedEncodingException, FileOpenException {
        byte[] bytes = "a号bc有".getBytes("gb2312");
        
        Gbk detector = new Gbk(bytes);
        DetectResult dr = detector.detect().result();
        legalResult(dr);
    }
    
    private void legalResult(DetectResult dr) {
        assertThat("failed should be zero", dr.getFailedNumber(), is(0));
        assertThat("percent should be 100", dr.successPercent(), is(100));
        assertThat("should detect 3 asciis", dr.getAsciiNumber(), is(3));
        assertThat("should detect 2 chinese", dr.getCharNumber(), is(2));
    }
    
    @Test
    public void illegal() throws UnsupportedEncodingException, FileOpenException {
        byte[] bytes = "a号bc有".getBytes("utf-8");
        
        Gbk detector = new Gbk(bytes);
        DetectResult dr = detector.detect().result();
        illegalResult(dr);
    }
    
    
    private void illegalResult(DetectResult dr) {
        assertThat("failed should be 0", dr.getFailedNumber(), is(0)); //gbk may compatible with utf-8
        assertThat("percent should be 100", dr.successPercent(), is(100));
        assertThat("should detect 2 asciis", dr.getAsciiNumber(), is(2));
        assertThat("should detect 3 chinese", dr.getCharNumber(), is(3));
    }
    
}
