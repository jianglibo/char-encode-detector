/**
 * Copyright 2015 jianglibo@gmail.com
 *
 */
package com.m3958.encode.detector;

import java.util.HashSet;
import java.util.Set;

/**
 * @author jianglibo@gmail.com
 *         2015年12月10日
 *
 */
public enum LanguageName {
    CHINESE("GB2312", "GBK"),
    UTF("UTF-8", "UTF-16"),
    TRADITIONAL_CHINESE("BIG5");
    
    private final Set<String> encodes;
    
    private LanguageName(String... encodesAry) {
        this.encodes = new HashSet<String>();
        for(String str: encodesAry) {
            this.encodes.add(str);
        }
    }
    
    public Set<String> getEncodes() {
        return encodes;
    }
    
}
