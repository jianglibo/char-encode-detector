package com.m3958.encode.detector.util;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 * @author jianglibo@gmail.com
 *         2015年12月4日
 *
 */
public class ByteUtil {

    final private static char[] hexArray = "0123456789ABCDEF".toCharArray();
    
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4]; // right shift 4 bytes. append zero on left.
            hexChars[j * 2 + 1] = hexArray[v & 0x0F]; // bitwise and.
        }
        return new String(hexChars);
    }
    
    /**
     * copy from google guava library
     * @param arrays
     * @return
     */
    public static byte[] concat(byte[]... arrays) {
        int length = 0;
        for (byte[] array : arrays) {
          length += array.length;
        }
        byte[] result = new byte[length];
        int pos = 0;
        for (byte[] array : arrays) {
          System.arraycopy(array, 0, result, pos, array.length);
          pos += array.length;
        }
        return result;
    }
}
