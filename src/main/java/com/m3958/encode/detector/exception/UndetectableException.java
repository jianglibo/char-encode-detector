package com.m3958.encode.detector.exception;

import com.m3958.encode.detector.Detector;

public class UndetectableException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 8647858194949230285L;
    
    private Detector detector;
    
    public UndetectableException(Detector dc) {
        this.detector = dc;
    }

    public Detector getDetector() {
        return detector;
    }

    public void setDetector(Detector detector) {
        this.detector = detector;
    }

}
