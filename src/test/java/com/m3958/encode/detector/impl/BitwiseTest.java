package com.m3958.encode.detector.impl;

import java.util.BitSet;

import org.junit.Test;

import com.m3958.encode.detector.BaseTe;

/**
 * @author jianglibo@gmail.com
 *         2015年12月7日
 *
 */
public class BitwiseTest extends BaseTe{

    @Test
    public void leadingones() {
        byte[] ba = new byte[]{0x3, 0x7, 0x15, 0x23};
        BitSet bs = BitSet.valueOf(ba);
        printme(bs.toString());
        
        byte mask6 = (byte)0xFC; 
        byte mask5 = (byte)0xF8;
        byte mask4 = (byte)0xF0;
        byte mask3 = (byte)0xE0;
        byte mask2 = (byte)0xC0;
        byte b = (byte)0xFF;
        printme(mask6 & b);
    }
}
