package com.m3958.encode.detector.util;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.m3958.encode.detector.util.ReverseFileReader;

/**
 * @author jianglibo@gmail.com
 *         2015年12月4日
 *
 */
public class ReverseFileReaderTest {
    
    

    @Test
    public void tShortLine() {
        assertThat("last line is right", readLastLine("hello.txt", 1).get(0), is("txt"));
        assertThat("last line is right", readLastLine("hello.txt", 2).get(0), is("hello"));
    }
    
    @Test
    public void tLongLine() {
        List<String> lines = readLastLine("longtext.txt", 4);
        assertThat("last line is right", lines.get(0), is("6688"));
        assertThat("last line is right", lines.get(1), is("33454"));
        assertThat("last line is right", lines.get(2), is("1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"));
    }
    
    private List<String> readLastLine(String fn, int lineNum) {
        Path file = Paths.get("fixtures", "inputs", fn); 
        ReverseFileReader rfr = new ReverseFileReader(file);
        List<byte[]> lines = rfr.readLines(lineNum);
        List<String> strLines = new ArrayList<String>();
        for (byte[] ba: lines) {
            strLines.add(new String(ba));
        }
        return strLines;
    }
}
