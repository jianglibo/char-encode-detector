package com.m3958.encode.detector;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.junit.Test;

import com.m3958.encode.detector.exception.UndetectableException;

/**
 * @author jianglibo@gmail.com
 *         2015年12月7日
 *
 */
public class DetectorsTest extends BaseTe {

    @Test
    public void t() throws UndetectableException {
        byte[] bytes = "宽松电视剧3dsj\n\r".getBytes(StandardCharsets.UTF_8);
        DetectResult dr =  Detectors.create().detect(bytes).result();
        assertThat(dr.getCharsetName(), is("UTF-8"));
        
        bytes = "宽松电视剧3dsj\n\r汉子‘○’".getBytes(Charset.forName("GBK"));
        dr =  Detectors.create().detect(bytes).result();
        assertThat(dr.getCharsetName(), is("GBK"));
    }
}
