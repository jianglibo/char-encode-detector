package com.m3958.encode.detector.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.IntStream;

import org.junit.Assume;
import org.junit.Test;

import com.m3958.encode.detector.AbstractDetector;
import com.m3958.encode.detector.BaseTe;
import com.m3958.encode.detector.util.ByteUtil;

/**
 * Unicode transformation format (UTF)
 * UTF 和 unicode是两回事
 * 
 * @author jianglibo@gmail.com
 *
 */

public class CharsetTest extends BaseTe {

    /**
     * 虽然标准 ASCII 码是 7 位编码，但由于计算机基本处理单位为字节（ 1byte = 8bit ），所以一般仍以一个字节来存放一个 ASCII 字符。每一个字节中多余出来的一位（最高位）在计算机内部通常保持为 0 （在数据传输时可用作奇偶校验位）。
     * 
     * 除了utf-16，32之外，其它的所有字符集对于A的编码都是1byte.
     */
    @Test
    public void all() {
        Map<String, Charset> all = Charset.availableCharsets();
        printme("charset number", all.size());

        Set<String> charsetNotSupportEncode = new HashSet<String>(); // 不支持

        Set<String> charsetCanNotEncodeA = new HashSet<String>(); // 不能encode A

        Set<String> charsetEncodedAGreatThanOneByte = new HashSet<String>();

        char a = 'A';
        CharBuffer cb = CharBuffer.allocate(1);
        cb.put(a);

        for (Entry<String, Charset> entry : all.entrySet()) {
            Charset c = entry.getValue();
            if (c.canEncode()) {
                CharsetEncoder encoder = c.newEncoder();
                cb.rewind();
                try {
                    if (encoder.canEncode(a)) {
                        ByteBuffer bb = encoder.encode(cb);
                        if (bb.limit() > 1) {
                            charsetEncodedAGreatThanOneByte.add(c.name());
                        }
                    } else {
                        charsetCanNotEncodeA.add(c.name());
                    }
                } catch (Exception e) {
                    printme(c.name(), "cannot encode ascii A.");
                }
            } else {
                charsetNotSupportEncode.add(c.name());
            }
        }
        printme("not support encode", charsetNotSupportEncode);
        printme("ascii byte more than one", charsetEncodedAGreatThanOneByte);
        printme("cannot encode A", charsetCanNotEncodeA);
    }

    /**
     * The set of characters from U+0000 to U+FFFF is sometimes referred to as the Basic Multilingual Plane (BMP).
     */
    @Test
    public void c() {
        assertThat(Character.isLetter('\uD840'), is(false));
        assertThat(Character.isLetter(0x2F81A), is(true));
    }

    /**
     * big endian, 最小的地址存最有影响的位， 90 - AB - 12 - CD
     * little endian: 最大的地址存最有影响的位： CD - 12 - AB - 90
     * 
     * “汉”字的Unicode编码是U+00006C49，然后把U+00006C49通过UTF-8编码器进行编码，最后输出的UTF-8编码是E6B189。
     * 
     * @throws CharacterCodingException
     */
    @Test
    public void littleBigEndian() throws CharacterCodingException {

        char h = '\u6C49';

        CharBuffer cb = CharBuffer.allocate(1);
        cb.put(h);

        cb.flip();

        CharsetEncoder encoder = StandardCharsets.UTF_8.newEncoder();
        ByteBuffer bb = encoder.encode(cb);
        byte[] ba = new byte[bb.limit()];
        bb.get(ba);
        printme(ByteUtil.bytesToHex(ba));

        printme(h); // 汉
    }

    /**
     * 7位（bits）表示一个字符，共128字符，字符值从0到127(7F)，其中32到126是可打印字符。
     * 
     * @throws IOException
     */
    @Test
    public void gb2312() throws IOException {
        Path path = Paths.get("c:", "charsets", "gb2312.txt");
        Assume.assumeTrue("fixture folder should exists", Files.exists(path));
        byte[] allbytes = Files.readAllBytes(path);

        int asciiNum = 0;

        for (byte bt : allbytes) {
            if (bt >= AbstractDetector.ZERO && bt < AbstractDetector.ONE27) {
                asciiNum++;
            }
        }

        printme("ascii number is", asciiNum);
    }

    @Test
    public void unicode() {
        byte b = 0;
        char first = '\u0000';
        int i = 0;
        printme("...............ascii start...........");
        for (; i < 128; i++) {
            byte[] ba = { (byte) i };
            printme(new String(ba), ByteUtil.bytesToHex(ba));
        }
        printme("...............ascii end...........");
    }

}
