package com.m3958.encode.detector.impl;

import java.nio.file.Path;

import com.m3958.encode.detector.AbstractDetector;
import com.m3958.encode.detector.Detector;
import com.m3958.encode.detector.LanguageName;

public class Gbk extends AbstractDetector implements Detector {

    public Gbk() {
    }

    public Gbk(byte[] bytes) {
        super(bytes);
    }

    public Gbk(Path path) {
        super(path);
    }

    public static final byte GBK_FRONT_BOTTOM = (byte) 0x81; // -127
    public static final byte GBK_FRONT_TOP = (byte) 0xFE; // -2

    public static final byte GBK_REAR_BOTTOM_1 = (byte) 0x40; // 64
    public static final byte GBK_REAR_TOP_1 = (byte) 0x7E; // 126

    public static final byte GBK_REAR_BOTTOM_2 = (byte) 0x80; // -128
    public static final byte GBK_REAR_TOP_2 = (byte) 0xFE; // -2

    protected boolean isGbkFirstByte(byte b) {
        if (b == GBK_FRONT_BOTTOM || b == GBK_FRONT_TOP) {
            return true;
        }
        return b > GBK_FRONT_BOTTOM && b < GBK_FRONT_TOP;
    }

    protected boolean isGbkSecondByte(byte b) {
        if (b == GBK_REAR_BOTTOM_1 || b == GBK_REAR_TOP_1 || b == GBK_REAR_TOP_1 || b == GBK_REAR_TOP_2) {
            return true;
        }
        return (b > GBK_REAR_BOTTOM_1 && b < GBK_REAR_TOP_1) || (b > GBK_REAR_BOTTOM_2 && b < GBK_REAR_TOP_2);
    }

    @Override
    public int maxCharBytes() {
        return 2;
    }

    @Override
    protected int detectOne(byte... bytes) {
        int len = bytes.length;

        switch (len) {
        case 1:
            if (isAscii(bytes[0])) {
                successedNumber++;
                asciiNumber++;
                return 0;
            } else {
                return 1;
            }
        case 2:
            if (isGbkFirstByte(bytes[0]) && isGbkSecondByte(bytes[1])) {
                successedNumber++;
                charNumber++;
            } else if (isAscii(bytes[1])) {
                asciiNumber++;
                failedNumber++;
            } else {
                failedNumber++;
            }
            return 0;
        default:
            return 0;
        }
    }

    @Override
    protected String getCharsetName() {
        return "GBK";
    }

    /* (non-Javadoc)
     * @see com.m3958.encode.detector.AbstractDetector#getLanguageName()
     */
    @Override
    protected LanguageName getLanguageName() {
        return LanguageName.CHINESE;
    }

}
