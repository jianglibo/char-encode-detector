package com.m3958.encode.detector.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import org.junit.Test;

import com.m3958.encode.detector.BaseTe;
import com.m3958.encode.detector.DetectResult;
import com.m3958.encode.detector.exception.FileOpenException;
import com.m3958.encode.detector.util.ByteUtil;

public class Gb2312Test extends BaseTe {

    @Test
    public void t() throws CharacterCodingException {

        char h = '\u6C49'; // 汉

        CharBuffer cb = CharBuffer.allocate(1);
        cb.put(h);
        cb.flip();

        Charset gb2312 = Charset.forName("gb2312");

        CharsetEncoder encoder = gb2312.newEncoder();
        ByteBuffer bb = encoder.encode(cb);

        assertThat("gb2312 is two byte.", bb.limit(), is(2));

        byte[] ba = new byte[bb.limit()];
        bb.get(ba);
        assertThat(ByteUtil.bytesToHex(ba), is("BABA"));

        Gb2312 detector = new Gb2312(ba);

        assertThat("byte value should in gb2312 range.", detector.isGb2312FirstByte(ba[0]), is(true));
        assertThat("byte value should in gb2312 range.", detector.isGb2312SecondByte(ba[1]), is(true));
    }

    @Test
    public void legal() throws UnsupportedEncodingException, FileOpenException {
        byte[] bytes = "a号bc有".getBytes("gb2312");

        Gb2312 detector = new Gb2312(bytes);
        DetectResult dr = detector.detect().result();
        legalResult(dr);
    }

    @Test
    public void detectFile() throws IOException, FileOpenException {
        Path path = fixtureOut.resolve("gb2312.txt");
        Writer wt = Files.newBufferedWriter(path, Charset.forName("gb2312"), StandardOpenOption.WRITE,
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
        
        wt.write("a号bc有");
        wt.close();
        
        Gb2312 detector = new Gb2312(path);
        DetectResult dr = detector.detect().result();
        legalResult(dr);
    }
    
    @Test
    public void detectFileLarge() throws IOException, FileOpenException {
        Path path = fixtureOut.resolve("gb2312.txt");
        Writer wt = Files.newBufferedWriter(path, Charset.forName("gb2312"), StandardOpenOption.WRITE,
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
        int writed = 0;
        
        while (writed < 10000) {
            wt.write("a号bc有");
            writed++;
        }
        wt.flush();
        wt.close();
        
        Gb2312 detector = new Gb2312(path);
        DetectResult dr = detector.scanWholeFile().detect().result();
        assertThat("success percent should be 100%", dr.successPercent(), is(100));
    }

    @Test
    public void illegal() throws UnsupportedEncodingException, FileOpenException {
        byte[] bytes = "a号bc有".getBytes("utf-8");

        Gb2312 detector = new Gb2312(bytes);
        DetectResult dr = detector.detect().result();
        illegalResult(dr);
    }

    private void illegalResult(DetectResult dr) {
        assertThat("failed should be 3", dr.getFailedNumber(), is(3));
        assertThat("percent should be 40", dr.successPercent(), is(40));
        assertThat("should detect 3 asciis", dr.getAsciiNumber(), is(3));
        assertThat("should detect 0 chinese", dr.getCharNumber(), is(0));
    }

    private void legalResult(DetectResult dr) {
        assertThat("failed should be zero", dr.getFailedNumber(), is(0));
        assertThat("percent should be 100", dr.successPercent(), is(100));
        assertThat("should detect 3 asciis", dr.getAsciiNumber(), is(3));
        assertThat("should detect 2 chinese", dr.getCharNumber(), is(2));
    }
}
